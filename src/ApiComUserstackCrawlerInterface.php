<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use DateTimeInterface;
use Stringable;

/**
 * ApiComUserstackCrawlerInterface interface file.
 * 
 * This represents the crawler information about the user agent.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUserstackCrawlerInterface extends Stringable
{
	
	/**
	 * Gets whether this user agent represents a crawler.
	 * 
	 * @return bool
	 */
	public function hasIsCrawler() : bool;
	
	/**
	 * Gets the category of the crawler.
	 * 
	 * @return string
	 */
	public function getCategory() : string;
	
	/**
	 * Gets the last seen date of the crawler.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getLastSeen() : ?DateTimeInterface;
	
}
