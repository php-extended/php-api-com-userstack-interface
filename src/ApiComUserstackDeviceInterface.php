<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiComUserstackDeviceInterface interface file.
 * 
 * This represents a device information about the user agent.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUserstackDeviceInterface extends Stringable
{
	
	/**
	 * Gets whether this device is a mobile.
	 * 
	 * @return bool
	 */
	public function hasIsMobileDevice() : bool;
	
	/**
	 * Gets the type of device.
	 * 
	 * @return string
	 */
	public function getType() : string;
	
	/**
	 * Gets the name of the brand.
	 * 
	 * @return string
	 */
	public function getBrand() : string;
	
	/**
	 * Gets the code of the brand.
	 * 
	 * @return string
	 */
	public function getBrandCode() : string;
	
	/**
	 * Gets the url of the brand.
	 * 
	 * @return ?UriInterface
	 */
	public function getBrandUrl() : ?UriInterface;
	
	/**
	 * Gets the name of the device model.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
