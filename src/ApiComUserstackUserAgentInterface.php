<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiComUserstackUserAgentInterface interface file.
 * 
 * This represents the user agent base data.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUserstackUserAgentInterface extends Stringable
{
	
	/**
	 * Gets the requested user agent.
	 * 
	 * @return string
	 */
	public function getUa() : string;
	
	/**
	 * Gets the type of software.
	 * 
	 * @return ?string
	 */
	public function getType() : ?string;
	
	/**
	 * Gets the vendor of the software.
	 * 
	 * @return ?string
	 */
	public function getBrand() : ?string;
	
	/**
	 * Gets the name of the software.
	 * 
	 * @return ?string
	 */
	public function getName() : ?string;
	
	/**
	 * Gets the url of the software.
	 * 
	 * @return ?UriInterface
	 */
	public function getUrl() : ?UriInterface;
	
	/**
	 * Gets this represents the operating system information about an user
	 * agent.
	 * 
	 * @return ?ApiComUserstackOperatingSystemInterface
	 */
	public function getOs() : ?ApiComUserstackOperatingSystemInterface;
	
	/**
	 * Gets this represents a device information about the user agent.
	 * 
	 * @return ?ApiComUserstackDeviceInterface
	 */
	public function getDevice() : ?ApiComUserstackDeviceInterface;
	
	/**
	 * Gets this represents the browser information about the user agent.
	 * 
	 * @return ?ApiComUserstackBrowserInterface
	 */
	public function getBrowser() : ?ApiComUserstackBrowserInterface;
	
	/**
	 * Gets this represents the crawler information about the user agent.
	 * 
	 * @return ?ApiComUserstackCrawlerInterface
	 */
	public function getCrawler() : ?ApiComUserstackCrawlerInterface;
	
}
