<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use InvalidArgumentException;
use PhpExtended\DataProvider\UnprovidableThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiComUserstackEndpointInterface class file.
 * 
 * This class acts as an endpoint for the userstack.com API.
 * 
 * @author Anastaszor
 */
interface ApiComUserstackEndpointInterface extends Stringable
{
	
	public const ERR_MISSING_ACCESS_KEY = 'missing_access_key';
	public const ERR_INVALID_ACCESS_KEY = 'invalid_access_key';
	public const ERR_INACTIVE_USER = 'inactive_user';
	public const ERR_INVALID_API_FUNCTION = 'invalid_api_function';
	public const ERR_USAGE_LIMIT_REACHED = 'usage_limit_reached';
	public const ERR_FUNCTION_ACCESS_RESTRICTED = 'function_access_restricted';
	public const ERR_HTTPS_ACCESS_RESTRICTED = 'https_access_restricted';
	public const ERR_MISSING_USER_AGENT = 'missing_user_agent';
	public const ERR_INVALID_FIELDS = 'invalid_fields';
	public const ERR_TOO_MANY_USER_AGENTS = 'too_many_user_agents';
	public const ERR_BATCH_NOT_SUPPORTED_ON_PLAN = 'batch_not_supported_on_plan';
	public const ERR_404_NOT_FOUND = '404_not_found';
	
	/**
	 * Gets the user agent object parsed from the given user agent string.
	 * 
	 * @param string $userAgentString
	 * @return ApiComUserstackUserAgentInterface
	 * @throws InvalidArgumentException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 * @throws RuntimeException if the endpoint cannot be joined
	 */
	public function getUserAgent(string $userAgentString) : ApiComUserstackUserAgentInterface;
	
}
