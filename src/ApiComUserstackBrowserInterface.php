<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use PhpExtended\Version\VersionInterface;
use Stringable;

/**
 * ApiComUserstackBrowserInterface interface file.
 * 
 * This represents the browser information about the user agent.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUserstackBrowserInterface extends Stringable
{
	
	/**
	 * Gets the name of the browser.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the version of the browser.
	 * 
	 * @return VersionInterface
	 */
	public function getVersion() : VersionInterface;
	
	/**
	 * Gets the major version of the browser.
	 * 
	 * @return int
	 */
	public function getVersionMajor() : int;
	
	/**
	 * Gets the engine of the browser.
	 * 
	 * @return string
	 */
	public function getEngine() : string;
	
}
