<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-com-userstack-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiComUserstack;

use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiComUserstackOperatingSystemInterface interface file.
 * 
 * This represents the operating system information about an user agent.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiComUserstackOperatingSystemInterface extends Stringable
{
	
	/**
	 * Gets the name of the operating system.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the code of the operating system.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the url of presentation about the operating system.
	 * 
	 * @return UriInterface
	 */
	public function getUrl() : UriInterface;
	
	/**
	 * Gets the family of the operating system.
	 * 
	 * @return string
	 */
	public function getFamily() : string;
	
	/**
	 * Gets the family code of the operating system.
	 * 
	 * @return string
	 */
	public function getFamilyCode() : string;
	
	/**
	 * Gets the family vendor of the operating system.
	 * 
	 * @return string
	 */
	public function getFamilyVendor() : string;
	
	/**
	 * Gets the icon image of the operating system.
	 * 
	 * @return UriInterface
	 */
	public function getIcon() : UriInterface;
	
	/**
	 * Gets the large icon image of the operating system.
	 * 
	 * @return UriInterface
	 */
	public function getIconLarge() : UriInterface;
	
}
